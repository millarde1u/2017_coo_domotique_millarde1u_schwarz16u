package Application;

import java.util.ArrayList;
import java.util.List;

public class Telecommande {

	private List<Peripherique> peripheriques;
	
	public Telecommande() {
		peripheriques=new ArrayList<Peripherique>();
	}
	
	public void ajouterLampe(Peripherique light) {
		peripheriques.add(light);
	}
	
	public void activerLampe(int indiceLampe) {
		if (indiceLampe < peripheriques.size())
		peripheriques.get(indiceLampe).allumer();
	}
	
	public void desactiverLampe(int indiceLampe) {
		if (indiceLampe < peripheriques.size())
		peripheriques.get(indiceLampe).eteindre();
	}
	
	public void activerTout() {
		for(Peripherique n:peripheriques){
			n.allumer();
		}
	}
	
	public List<Peripherique> getLampes(){
		return this.peripheriques;
	}
	
	public String toString() {
		String s=new String();
		for(int i=0; i<peripheriques.size();i++){
			s = "(" + peripheriques.get(i) + ")";
			if(i!=peripheriques.size()-1){
				s+=",";
			}
		}
		return s;
	}
	
}