package Application;

public class AdapterLumiere implements Peripherique{
	
	private Lumiere lumiereControlee;
	
	public AdapterLumiere(Lumiere r){
		lumiereControlee=r;
	}

	@Override
	public void allumer() {
		lumiereControlee.changerIntensite(20);
	}

	@Override
	public void eteindre() {
		lumiereControlee.changerIntensite(00);
		
	}

	@Override
	public boolean isAllume() {
		if (lumiereControlee.getLumiere() == 20){
			return true;
		}
		return false;
	}
	
	
}
