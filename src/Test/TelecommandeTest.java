package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Application.*;

public class TelecommandeTest {

	@Test
	public void testAjouterLampeTelecommandeVide() {
		Telecommande tel = new Telecommande();
		Lampe l = new Lampe("light");
		
		tel.ajouterLampe(l);
		
		assertEquals("La lampe devrait �tre ajout�e dans la liste des lampes que la telecommande contr�le.",l, tel.getLampes().get(0));
	}

	@Test
	public void testAjouterLampeTelecommandeUnElement() {
		Telecommande tel = new Telecommande();
		Lampe l = new Lampe("light");
		Lampe l2 = new Lampe("light2");
		
		tel.ajouterLampe(l);
		tel.ajouterLampe(l2);
		
		assertEquals("La deuxieme lampe devrait etre ajoutee dans la liste des lampes que la telecommande commande", l2, tel.getLampes().get(1));
	}
	
	@Test
	public void testActiverLampeExistantePositionZero() {
		Telecommande tel = new Telecommande();
		Lampe l = new Lampe("light");
		Lampe l2 = new Lampe("light2");
		
		tel.ajouterLampe(l);
		tel.ajouterLampe(l2);
		tel.activerLampe(0);
		
		assertEquals("La deuxieme lampe devrait etre ajoutee dans la liste des lampes que la telecommande commande", l2, tel.getLampes().get(1));
		assertEquals("La lampe en position 0 devrait etre allumée", true, tel.getLampes().get(0).isAllume());
	}
	
	@Test
	public void testActiverLampeExistantePositionUne() {
		Telecommande tel = new Telecommande();
		Lampe l = new Lampe("light");
		Lampe l2 = new Lampe("light2");
		
		tel.ajouterLampe(l);
		tel.ajouterLampe(l2);
		tel.activerLampe(1);
		
		assertEquals("La deuxieme lampe devrait etre ajoutee dans la liste des lampes que la telecommande commande", l2, tel.getLampes().get(1));
		assertEquals("La lampe en position 0 devrait etre allumée",true, tel.getLampes().get(1).isAllume());
	}
	
	@Test
	public void testActiverLampeInexistante() {
		Telecommande tel = new Telecommande();
		Lampe l = new Lampe("light");
		Lampe l2 = new Lampe("light2");
		
		tel.ajouterLampe(l);
		tel.ajouterLampe(l2);
		tel.activerLampe(3);
		
		assertEquals("La lampe inexistante ne devrait pas modifier l'etat de la lampe, la liste devrait etre de 2 mem", 2, tel.getLampes().size());
	}
	
}